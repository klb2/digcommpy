import matplotlib.pyplot as plt

from digcommpy import messages
from digcommpy import encoders
from digcommpy import channels
from digcommpy import decoders
from digcommpy import metrics

def main(n, k):
    err_probs = [.01, 0.05, 0.1, 0.15, 0.2]
    ber = []
    mess = messages.generate_data(k, number=1000, binary=True)
    for p in err_probs:
        print("Simulating bit flip prob p={:2f}".format(p))
        enc = encoders.PolarEncoder(n, k, 'BSC', p)
        cha = channels.BscChannel(p)
        dec = decoders.PolarDecoder(n, k, 'BSC', p)
        code = enc.encode_messages(mess)
        rec = cha.transmit_data(code)
        est = dec.decode_messages(rec, cha)
        ber.append(metrics.ber(mess, est))
    plt.plot(err_probs, ber, 'o-')
    plt.plot(err_probs, err_probs, 'r--')
    plt.xlabel("Bit Flip Probability")
    plt.ylabel("BER")

if __name__ == "__main__":
    n = 16
    k = 5
    main(n, k)
    plt.show()
