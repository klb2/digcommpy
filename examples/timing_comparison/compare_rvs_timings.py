import time

from scipy import stats

from digcommpy import information_theory as it

def main(num_samples):
    means = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [0, -1, -2], [-3, -4, -5]]
    noise_var = 1.
    gauss_mix = it.GaussianMixtureRv(means, noise_var)
    dists = {"Gauss Mix": gauss_mix, "Normal": stats.norm(),
             "Multi Normal": stats.multivariate_normal(means[0]),
             "Uniform": stats.uniform()}
    print("Comparing RVS generation for different distributions.")
    print("Generating {:d} samples each".format(num_samples))
    print("{:10}\tTime (s)\n{}".format("Distribution", "-"*25))
    for name, dist in dists.items():
        time_start = time.time()
        dist.rvs(num_samples)
        time_end = time.time()
        print("{:10}\t{:.3f}".format(name, time_end-time_start))


if __name__ == "__main__":
    main(num_samples=10000)
