import time

from digcommpy import messages

def main(number=1000000):
    generators = {"binary8": [8, number, True],
                  "binary16": [16, number, True],
                  "dec8": [8, number, False],
                  "dec16": [16, number, False],
                }
    print("Comparing different message generating speeds.")
    print("Testing {:.2E} messages".format(number))
    print("{:10}\tTime (s)\n{}".format("Channel", "-"*25))
    for name, generator in generators.items():
        time_start = time.time()
        output = messages.generate_data(*generator)
        time_end = time.time()
        print("{:10}\t{:.3f}".format(name, time_end-time_start))

if __name__ == "__main__":
    main()
