import time

import numpy as np
from digcommpy import decoders
from digcommpy import channels

def main(code_length=32, info_length=8, number=100000):
    codewords = np.random.randn(number, code_length)
    _decoders = {"identity": decoders.IdentityDecoder,
                 "polar": decoders.PolarDecoder(code_length, info_length, "BAWGN", 0.),
                 "polar_wt": decoders.PolarWiretapDecoder(code_length, "BAWGN", "BAWGN", 0, -5),
                }
    print("Comparing different decoder speeds.")
    print("Decoding {:.2E} messages with n={}, k={}".format(number, code_length, info_length))
    print("{:10}\tTime (s)\n{}".format("Decoder", "-"*25))
    for name, decoder in _decoders.items():
        time_start = time.time()
        messages = decoder.decode_messages(codewords, channels.BawgnChannel(-10))
        time_end = time.time()
        print("{:10}\t{:.3f}".format(name, time_end-time_start))

if __name__ == "__main__":
    main()
