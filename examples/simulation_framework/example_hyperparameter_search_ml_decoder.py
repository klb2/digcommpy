from digcommpy import encoders
from digcommpy import channels
from digcommpy import decoders
from digcommpy import simulations

n, k = 16, 4                                                   
test_size = 1000                                               
#encoder = encoders.PolarEncoder(n, k, "BAWGN", 0.)
encoder = encoders.PolarWiretapEncoder(n, "BAWGN", "BAWGN", 0., -5)
decoder = decoders.NeuralNetDecoder
channel = channels.BawgnChannel                                
cha_var = {'snr_db': [-5, 0, 5]}                               
dec_var = {'layer': ([16, 8], [128]), 'train_snr': (-5, 5)}    
simulation = simulations.HyperparameterSearchDecoderSimulation(
    encoder, decoder, channel, dec_var, cha_var)               
training_opt = {'epochs': 10}                                  
results = simulation.start_simulation(test_size, training_options=training_opt)
