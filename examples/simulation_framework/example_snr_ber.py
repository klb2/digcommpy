from digcommpy.simulations import ChannelParameterSimulation
from digcommpy.channels import BawgnChannel
from digcommpy.encoders import PolarEncoder
from digcommpy.decoders import PolarDecoder
from digcommpy.modulators import BpskModulator

def main():
    n, k = 16, 8
    test_size = 10000
    design_snr = 0.
    encoder = PolarEncoder(n, k, "BAWGN", design_snr)
    decoder = PolarDecoder(n, k, "BAWGN", design_snr)
    channel = BawgnChannel(design_snr, rate=k/n)
    test_snr = range(-5, 6)
    simulation = ChannelParameterSimulation(encoder, decoder, channel,
                                            modulator=BpskModulator())
    results = simulation.simulate(test_snr, test_size)
    print(results)

if __name__ == "__main__":
    main()
