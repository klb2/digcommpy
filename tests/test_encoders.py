import numpy as np
import pytest

from digcommpy.messages import generate_data
from digcommpy import encoders
from digcommpy import checks
from digcommpy import channels

def test_identity_encoder():
    messages = np.array([[0, 1], [1, 0], [0, 0], [1, 1]])
    encoder = encoders.IdentityEncoder(code_length=2, info_length=2)
    codewords = encoder.encode_messages(messages)
    assert np.all(codewords == messages)

def test_linear_encoder_binary():
    messages = np.array([[0, 1], [1, 0], [0, 0], [1, 1]])
    matrix = np.array([[1, 1, 0], [0, 1, 1]])
    encoder = encoders.LinearEncoder(matrix, base=2)
    codewords = encoder.encode_messages(messages)
    expected = np.array([[0, 1, 1], [1, 1, 0], [0, 0, 0], [1, 0, 1]])
    assert np.all(codewords == expected)

def test_linear_encoder_tenary():
    messages = np.array([[0, 1], [2, 1], [0, 0], [2, 0]])
    matrix = np.array([[1, 1, 0], [0, 2, 1]])
    encoder = encoders.LinearEncoder(matrix, base=3)
    codewords = encoder.encode_messages(messages)
    expected = np.array([[0, 2, 1], [2, 1, 1], [0, 0, 0], [2, 2, 0]])
    assert np.all(codewords == expected)

def test_polar_construction_awgn():
    pos_lookup = encoders.PolarEncoder.construct_polar_code(4, 2, "BAWGN", 0)
    info_pos = np.where(pos_lookup == -1)
    expected = [1, 3]
    assert all(info_pos[0] == expected)

@pytest.mark.parametrize("channel", [channels.BawgnChannel(0), 
    channels.BscChannel(.1), channels.BecChannel(.1)])
def test_polar_encoder_construction_channel_object(channel):
    encoder = encoders.PolarEncoder(4, 2, channel)
    info_pos = np.where(encoder.pos_lookup == -1)
    expected = [1, 3]
    assert all(info_pos[0] == expected)

def test_polar_single_encoding():
    message = [0, 1]
    pos_lookup = [-1, 0, 0, -1]
    expected = [1, 1, 1 ,1]
    encoder = encoders.PolarEncoder(4, 2, "BAWGN")
    code_word = encoder._encode_single_message(message, pos_lookup)
    assert all(code_word == expected)

@pytest.mark.parametrize("channel, param", [("BAWGN", 0), ("BSC", .3), ("BEC", .5)])
def test_polar_encoding(channel, param):
    N = 4
    k = 2
    encoder = encoders.PolarEncoder(N, k, channel, param)
    _messages = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    expected = np.array([[0, 0, 0, 0], [1, 1, 1, 1], [1, 1, 0, 0], [0, 0, 1, 1]])
    code_words = encoder.encode_messages(_messages)
    assert np.all(code_words == expected)

def test_repetition_encoding():
    encoder = encoders.RepetitionEncoder(3)
    _messages = [[1, 0], [0, 1]]
    code_words = encoder.encode_messages(_messages)
    expected = [[1, 1, 1], [0, 0, 0], [0, 0, 0], [1, 1, 1]]
    assert np.all(code_words == expected)


@pytest.mark.parametrize("parallel", [False, True])
@pytest.mark.slow
def test_polar_encoding_parallel(parallel):
    N = 64
    k = 16
    channel = "BAWGN"
    param = 0.
    encoder = encoders.PolarEncoder(N, k, channel, param, parallel=parallel)
    _messages = generate_data(k, number=1e3, binary=True)
    code_words = encoder.encode_messages(_messages)
    assert True


def test_polar_wiretap_encoder():
    N = 16
    bob = 0
    eve = -5
    channel = "BAWGN"
    encoder = encoders.PolarWiretapEncoder(N, channel, channel, bob, eve)
    k = encoder.info_length
    _messages = generate_data(k, number=1e3, binary=True)
    code_words = encoder.encode_messages(_messages)
    assert (k == 4) and (checks.is_binary_message(code_words))


def test_generate_codebook_linear():
    N = 4
    k = 2
    G = np.array([[1, 0, 0, 1], [0, 0, 1, 1]])
    encoder = encoders.LinearEncoder(G)
    codebook = encoder.generate_codebook()
    expected = np.array([[0, 0, 0, 0], [0, 0, 1, 1], [1, 0, 0, 1], [1, 0, 1, 0]])
    assert np.all(expected == codebook[1])

def test_generate_codebook_polar():
    N = 4
    k = 2
    encoder = encoders.PolarEncoder(N, k, "BAWGN")
    expected = np.array([[0, 0, 0, 0], [1, 1, 1, 1], [1, 1, 0, 0], [0, 0, 1, 1]])
    codebook = encoder.generate_codebook()
    assert np.all(codebook[1] == expected)

def test_generate_codebook_polar_wiretap():
    N = 8
    encoder = encoders.PolarWiretapEncoder(N, "BAWGN", "BAWGN", 0., -5.)
    codebook = encoder.generate_codebook()
    assert len(codebook[1]) == len(np.unique(codebook[1], axis=0))


@pytest.mark.parametrize("parallel", [True, False])
def test_codebook_encoder_dict(parallel):
    codebook = {0: [0, 1, 2], 1: [-1, 0, 3], 2: [4, 1, 2]}
    encoder = encoders.CodebookEncoder(3, 2, codebook, parallel=parallel)
    messages = np.array([[0, 0], [0, 1]])
    codewords = encoder.encode_messages(messages)
    assert np.all(codewords == np.array([[0, 1, 2], [-1, 0, 3]]))

@pytest.mark.parametrize("parallel", [True, False])
def test_codebook_encoder_tuple(parallel):
    codebook = (np.array([[0, 0], [0, 1], [1, 0]]),
                np.array([[0, 1, 2], [-1, 0, 3], [4, 1, 2]]))
    encoder = encoders.CodebookEncoder(3, 2, codebook, parallel=parallel)
    messages = np.array([[0, 0], [0, 1]])
    codewords = encoder.encode_messages(messages)
    assert np.all(codewords == np.array([[0, 1, 2], [-1, 0, 3]]))

@pytest.mark.parametrize("parallel", [True, False])
def test_codebook_encoder_file(parallel):
    codebook = "example_codebook.csv"
    encoder = encoders.CodebookEncoder(5, 2, codebook, parallel=parallel)
    messages = np.array([[0, 0], [0, 1]])
    codewords = encoder.encode_messages(messages)
    assert np.all(codewords == np.array([[1, 2, 3, 4, 5], [-1, -2, -3, -4, -5]]))
