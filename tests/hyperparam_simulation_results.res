{'code_length': 16, 'info_length': 4, 'random_length': 3, 'test_size': 1000}
{'layer': [16, 8], 'train_snr': -5}
{(('snr_db', -5),): {'ber': 0.497, 'bler': 0.936}, (('snr_db', 0),): {'ber': 0.48875, 'bler': 0.941}, (('snr_db', 5),): {'ber': 0.49575, 'bler': 0.941}}
{'layer': [16, 8], 'train_snr': 5}
{(('snr_db', -5),): {'ber': 0.49475, 'bler': 0.925}, (('snr_db', 0),): {'ber': 0.49125, 'bler': 0.925}, (('snr_db', 5),): {'ber': 0.48725, 'bler': 0.932}}
{'layer': [128], 'train_snr': -5}
{(('snr_db', -5),): {'ber': 0.51, 'bler': 0.956}, (('snr_db', 0),): {'ber': 0.508, 'bler': 0.959}, (('snr_db', 5),): {'ber': 0.496, 'bler': 0.961}}
{'layer': [128], 'train_snr': 5}
{(('snr_db', -5),): {'ber': 0.50675, 'bler': 0.964}, (('snr_db', 0),): {'ber': 0.5075, 'bler': 0.967}, (('snr_db', 5),): {'ber': 0.506, 'bler': 0.972}}
