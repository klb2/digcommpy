import pytest
import numpy as np

from digcommpy import demodulators

def test_bspk_demodulator():
    x = [[-1, -2, 0], [1, 3, 2.2]]
    expected = [[0, 0, 0], [1, 1, 1]]
    demod = demodulators.BpskDemodulator()
    result = demod.demodulate_symbols(x)
    assert np.all(result == expected)

def test_identity_demodulator():
    x = [-1, -2, 1.2, 0]
    demod = demodulators.IdentityDemodulator()
    result = demod.demodulate_symbols(x)
    assert all(result == x)

@pytest.mark.parametrize("messages, expected, m",
    [([[-1-1j, -1-1j], [1+1j, 1+1j], [1+1j, -1-1j]], [[0, 1, 0, 1], [1, 0, 1, 0], [1, 0, 0, 1]], 4),
     ([[-1+1j], [1-1j], [1+1j]], [[0, 1, 0, 1], [1, 0, 1, 0], [1, 0, 0, 1]], 16)])
def test_demodulation_qam(messages, expected, m):
    modulated_symbols = demodulators.QamDemodulator.demodulate_symbols(messages, m)
    assert np.all(modulated_symbols == expected)

@pytest.mark.parametrize("m, length", [(16, 6), (4, 3), (16, 2)])
def test_demodulation_qam_shape(m, length):
    messages = np.random.randn(2, length) + 1j*np.random.randn(2, length)
    expected_length = int(np.log2(m)*length)
    demod = demodulators.QamDemodulator.demodulate_symbols(messages, m)
    assert expected_length == np.shape(demod)[1]

@pytest.mark.parametrize("m", [3, 5, 8, 12])
def test_demodulation_qam_exception_m(m):
    with pytest.raises(ValueError):
        m = demodulators.QamDemodulator.demodulate_symbols([[1+1j, -1-1j, 1-1j, -1+1j]], m)
