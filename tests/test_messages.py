import pytest
import numpy as np

from digcommpy import messages


@pytest.mark.parametrize("info_bits, expected", [(1, [[0], [1]]),
                                                 (2, [[0, 0], [0, 1],
                                                      [1, 0], [1, 1]])])
def test_generate_all_info_words(info_bits, expected):
    _messages = messages.generate_data(info_bits, number=None, binary=True)
    assert all([list(k) in expected for k in _messages])


@pytest.mark.parametrize("info_bits, number, expected", [(1, 100, (100, 1)),
                                                         (5, 235, (235, 5)),
                                                         (10, 20, (20, 10))])
def test_generate_data_shape(info_bits, number, expected):
    _messages = messages.generate_data(info_bits, number=number, binary=True)
    assert _messages.shape == expected


def test_unpack_to_bits():
    _messages = np.array([5, 2, 7, 1], dtype=int)
    info_bits = 3
    expected = np.array([[1, 0, 1], [0, 1, 0], [1, 1, 1], [0, 0, 1]])
    bits = messages.unpack_to_bits(_messages, info_bits)
    assert np.all(bits == expected)


def test_pack_to_dec():
    bits = np.array([[1, 0, 1], [0, 1, 0], [1, 1, 1], [0, 0, 1]])
    expected = np.array([[5], [2], [7], [1]], dtype=int)
    _messages = messages.pack_to_dec(bits)
    assert np.all(_messages == expected)

@pytest.mark.slow
@pytest.mark.parametrize("number, info_bits", [(1000, 8), (100000, 8), (1000000, 8),
                                               (1000, 16), (100000, 16), (1000000, 16)])
def test_generate_messages_time(number, info_bits):
    _messages = messages.generate_data(info_bits, number=number, binary=False)

@pytest.mark.slow
@pytest.mark.parametrize("number, info_bits", [(1000, 8), (100000, 8), (1000000, 8),
                                               (1000, 16), (100000, 16), (1000000, 16)])
def test_unpack_to_bits_time(number, info_bits):
    _messages = messages.generate_data(info_bits, number=number, binary=False)
    _messages_bin = messages.unpack_to_bits(_messages, info_bits)

@pytest.mark.slow
@pytest.mark.parametrize("number, info_bits", [(1000, 8), (100000, 8), (1000000, 8),
                                               (1000, 16), (100000, 16), (1000000, 16)])
def test_pack_to_dec_time(number, info_bits):
    _messages_bin = messages.generate_data(info_bits, number=number, binary=True)
    _messages = messages.pack_to_dec(_messages_bin)
