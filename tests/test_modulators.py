import pytest
import numpy as np

from digcommpy import modulators

@pytest.mark.parametrize("messages, expected", [([[0]], [[-1]]),
                                                ([[1]], [[1]]),
                                                ([[1, 0], [1, 1]],
                                                 [[1, -1], [1, 1]])])
def test_modulation_bpsk(messages, expected):
    modulated = modulators.BpskModulator.modulate_symbols(messages)
    assert np.all(modulated == expected)

@pytest.mark.parametrize("messages, expected, m",
    [([[0, 1, 0, 1], [1, 0, 1, 0], [1, 0, 0, 1]], [[-1-1j, -1-1j], [1+1j, 1+1j], [1+1j, -1-1j]], 4),
     ([[0, 1, 0, 1], [1, 0, 1, 0], [1, 0, 0, 1]], [[-1+1j], [1-1j], [1+1j]], 16)])
def test_modulation_qam(messages, expected, m):
    modulated_symbols = modulators.QamModulator.modulate_symbols(messages, m)
    assert np.all(modulated_symbols == expected)

@pytest.mark.parametrize("m, length", [(16, 6), (4, 3), (16, 2)])
def test_modulation_qam_exception_shape(m, length):
    messages = np.random.randint(0, 2, (2, length))
    with pytest.raises(ValueError):
        m = modulators.QamModulator.modulate_symbols(messages, m)

@pytest.mark.parametrize("m", [3, 5, 8, 12])
def test_modulation_qam_exception_m(m):
    with pytest.raises(ValueError):
        m = modulators.QamModulator.modulate_symbols([[1, 0, 1, 0]], m)
