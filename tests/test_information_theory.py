import numpy as np
import pytest

from digcommpy import information_theory as it


def test_entropy():
    prob = [.125, .5, .25, .125]
    entr = it.entropy(prob)
    expected = 1.75
    assert entr == expected

@pytest.mark.parametrize("prob, expected", [(1, 0), (0, 0), (.5, 1)])
def test_binary_entropy(prob, expected):
    entr = it.binary_entropy(prob)
    assert entr == expected

def test_gauss_mix_entropy_upper():
    mu = [[-1, -5.4], [5, -2.6], [3, 3.4], [-3, .6]]
    sig = [[1.5, .35], [.35, 1.54]]
    expected = 4.6155
    calc = it.entropy_gauss_mix_upper(mu, sig)
    assert np.round(calc, decimals=4) == expected

def test_gauss_mix_entropy_lower():
    mu = [[-1, -5.4], [5, -2.6], [3, 3.4], [-3, .6]]
    sig = [[1.5, .35], [.35, 1.54]]
    expected = 4.5546
    calc = it.entropy_gauss_mix_lower(mu, sig)
    assert np.round(calc, decimals=4) == expected

def test_gauss_mix_pdf_dimensions():
    mu = [[1, -1], [1, 2]]
    sig = [[2, -.4], [-.4, 1]]
    gm = it.GaussianMixtureRv(mu, sig)
    test = [[1, 2], [-1, -.3]]
    expected = [0.05911, 0.02096]
    calc = gm.pdf(test)
    assert np.all(np.round(calc, decimals=5) == expected)

def test_gauss_mix_pdf():
    mu = [[1, -1], [1, 2]]
    sig = [[2, -.4], [-.4, 1]]
    gm = it.GaussianMixtureRv(mu, sig)
    test = [[0, 0]]
    expected = 0.0356
    calc = gm.pdf(test)
    assert np.round(calc, decimals=4) == expected

def test_gauss_mix_logpdf():
    mu = [[1, -1], [1, 2]]
    sig = [[2, -.4], [-.4, 1]]
    gm = it.GaussianMixtureRv(mu, sig)
    test = [[0, 0]]
    expected = -3.33659
    calc = gm.logpdf(test)
    assert np.round(calc, decimals=5) == expected

def test_gauss_mix_logpdf_dimensions():
    mu = [[1, -1], [1, 2]]
    sig = [[2, -.4], [-.4, 1]]
    gm = it.GaussianMixtureRv(mu, sig)
    test = [[1, 2], [-1, -.3]]
    expected = [-2.82842, -3.86504]
    calc = gm.logpdf(test)
    assert np.all(np.round(calc, decimals=5) == expected)

def test_gauss_mix_num_components():
    mu = [[1, -1], [1, 2], [-5, 0.]]
    gm = it.GaussianMixtureRv(mu)
    expected = 3
    assert len(gm) == expected

def test_gauss_mix_dimension():
    mu = [[1, 2, 3], [-4, -5, -6], [0, 1, 0], [5, 4, 1]]
    gm = it.GaussianMixtureRv(mu)
    expected = 3
    assert gm.dim() == expected

def test_gauss_mix_rvs():
    mu = [[1, -1], [1, 2]]
    sig = [[2, -.4], [-.4, 1]]
    weights = [.8, .2]
    gm = it.GaussianMixtureRv(mu, sig, weights)
    rv = gm.rvs(N=10)
    print(rv)
    assert np.shape(rv) == (10, 2)
