import numpy as np
import pytest

from digcommpy import metrics


@pytest.mark.parametrize("normalize, expected", [(True, .4), (False, 4)])
def test_ber(normalize, expected):
    x = np.array([[0, 1], [1, 0], [1, 1], [1, 0], [0, 0]])
    y = np.array([[1, 1], [1, 0], [1, 1], [0, 1], [1, 0]])
    ber = metrics.ber(x, y, normalize=normalize)
    assert ber == expected

@pytest.mark.parametrize("normalize, expected", [(True, .6), (False, 3)])
def test_bler(normalize, expected):
    x = np.array([[0, 1], [1, 0], [1, 1], [1, 0], [0, 0]])
    y = np.array([[1, 1], [1, 0], [1, 1], [0, 1], [1, 0]])
    bler = metrics.bler(x, y, normalize=normalize)
    assert bler == expected

@pytest.mark.parametrize("function", [metrics.ber, metrics.bler])
def test_error_rate_non_binary(function):
    x = np.array([[0, 1], [1, 0], [1, 1], [1, 0], [0, 0]])
    y = np.array([[1, np.nan], [1, 0], [1, 1], [np.nan, 2], [-1, 0]])
    with pytest.raises(ValueError):
        error_rate = function(x, y, normalize=True)
