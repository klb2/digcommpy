import pytest
import numpy as np

from digcommpy import checks


@pytest.mark.parametrize("message,expected", [([[1, 0], [0, 1], [1, 1]], True),
                                              ([[-1, 0], [1, 2]], False),
                                              ([[0, 1], [np.nan, 0]], False),
                                              ([[0, 1], [np.inf, 0]], False)])
def test_is_binary_message(message, expected):
    assert checks.is_binary_message(message) == expected
